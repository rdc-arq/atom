FROM ubuntu:18.04

ENV TZ America/Belem

ENV DEBIAN_FRONTEND noninteractive

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y supervisor apt-transport-https \
    unzip wget gnupg  \
    locales \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG=en_US.UTF-8

RUN useradd -ms /bin/bash atom

RUN apt-get install -y php7.2-cli php7.2-curl php7.2-json php7.2-ldap php7.2-mysql \
    php7.2-opcache php7.2-readline php7.2-xml php7.2-fpm php7.2-mbstring php7.2-xsl \
    php7.2-zip php-apcu php-memcache nginx fop libsaxon-java imagemagick ghostscript \
    poppler-utils ffmpeg gearman-job-server \
    && rm -rf /var/lib/apt/lists/*

COPY ./nginx/sites-available/atom /etc/nginx/sites-available/

RUN ln -sf /etc/nginx/sites-available/atom /etc/nginx/sites-enabled/atom \
    && rm /etc/nginx/sites-enabled/default

COPY ./php/7.2/fpm/pool.d/atom.conf /etc/php/7.2/fpm/pool.d/

RUN wget https://storage.accesstomemory.org/releases/atom-2.6.4.tar.gz \
    && mkdir /usr/share/nginx/atom \
    && tar xzf atom-2.6.4.tar.gz -C /usr/share/nginx/atom --strip 1 \
    && rm atom-2.6.4.tar.gz

RUN cp /usr/share/nginx/atom/config/propel.ini.tmpl /usr/share/nginx/atom/config/propel.ini \
    && cp /usr/share/nginx/atom/apps/qubit/config/settings.yml.tmpl /usr/share/nginx/atom/apps/qubit/config/settings.yml \
    && touch /usr/share/nginx/atom/config/databases.yml

RUN mkdir -p /usr/share/nginx/atom/transferdata/uploads \
    && mkdir -p /usr/share/nginx/atom/transferdata/downloads \
    && ln -sf /usr/share/nginx/atom/transferdata/uploads /usr/share/nginx/atom/uploads  \
    && ln -sf /usr/share/nginx/atom/transferdata/downloads /usr/share/nginx/atom/downloads

RUN chown -R www-data:www-data /usr/share/nginx/atom

WORKDIR /opt/atom

COPY ./supervisord.conf .

ENTRYPOINT ["/usr/bin/supervisord","-c","/opt/atom/supervisord.conf"]
